package initial

import (
	"time"

	"github.com/innopolis/labtest/helper"
	"github.com/innopolis/labtest/music"
)

var LastMotionTime time.Time

func HandleMusic(motionDetected bool) {
	time := time.Now()

	// Set last motion variable
	if motionDetected {
		LastMotionTime = time
	}

	// If motion was detected in the evening or night, turn the music on
	timeOfDay := helper.GetTimeOfDay(time)
	if motionDetected && (timeOfDay == "Evening" || timeOfDay == "Night") {
		music.TurnOn()
	} else {
		music.TurnOff()
	}
}
