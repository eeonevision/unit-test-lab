package party

import "time"

type FakeDateTimeProvider struct {
	time time.Time
}

func NewFakeDateTimeProvider(time time.Time) *FakeDateTimeProvider {
	return &FakeDateTimeProvider{
		time: time,
	}
}

func (fd *FakeDateTimeProvider) GetDateTime() time.Time {
	return fd.time
}
