package party

import (
	"fmt"
	"testing"
	"time"
)

func TestHandleMusicMotionDetected(t *testing.T) {
	// Arrange
	fakeDatetime := NewFakeDateTimeProvider(
		time.Date(2019, 03, 29, 23, 59, 00, 00, time.UTC),
	)
	party := NewParty(fakeDatetime)
	turnOn := func() {
		fmt.Print("Music is turned on")
	}
	turnOff := func() {
		fmt.Print("Music is turned off")
	}

	// Act
	party.HandleMusic(true, turnOn, turnOff)

	// Assert
	if time.Date(2019, 03, 29, 06, 59, 00, 00, time.UTC) != party.lastMotionTime {
		t.Fatal("Last motion time is not equal!")
	}
}
