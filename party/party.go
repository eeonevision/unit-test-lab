package party

import (
	"time"

	"github.com/innopolis/labtest/helper"
)

type IDateProvider interface {
	GetDateTime() time.Time
}

type Party struct {
	lastMotionTime time.Time
	dateProvider   IDateProvider
}

func NewParty(time IDateProvider) *Party {
	return &Party{
		dateProvider: time,
	}
}

func (p *Party) HandleMusic(motionDetected bool, turnOn func(), turnOff func()) {
	time := p.dateProvider.GetDateTime()

	// Set last motion variable
	if motionDetected {
		p.lastMotionTime = time
	}

	// If motion was detected in the evening or night, turn the music on
	timeOfDay := helper.GetTimeOfDay(time)
	if motionDetected && (timeOfDay == helper.Evening || timeOfDay == helper.Night) {
		turnOn()
	} else {
		turnOff()
	}
}
