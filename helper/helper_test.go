package helper

import (
	"testing"
	"time"
)

func TestGetTimeOfDayAt6AM(t *testing.T) {
	// Arrange is empty cause we in the same package, nothing to initialize

	// Act
	timeOfDay := GetTimeOfDay(time.Date(2019, 03, 29, 06, 00, 00, 00, time.UTC))

	//Assert
	if timeOfDay != "Morning" {
		t.Fatal("Expected: \"Morning\". Actual: " + timeOfDay)
	}

}
