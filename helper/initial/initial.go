package initial

import (
	"fmt"
	"time"
)

func GetTimeOfDay() string {
	now := time.Now()

	if now.Hour() >= 0 && now.Hour() < 6 {
		return "Night"
	}
	if now.Hour() >= 6 && now.Hour() < 12 {
		return "Morning"
	}
	if now.Hour() >= 12 && now.Hour() < 18 {
		return "Afternoon"
	}
	return "Evening"
}

func main() {
	fmt.Printf("Now is: %s", GetTimeOfDay())
}
