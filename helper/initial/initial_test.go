package initial

import "testing"

func TestGetTimeOfDayAt6AM(t *testing.T) {
	// Setup change system time to 6AM

	// Arrange is empty cause we in the same package, nothing to initialize

	// Act
	timeOfDay := GetTimeOfDay()

	//Assert
	if timeOfDay != "Morning" {
		t.Fatal("Expected: \"Morning\". Actual: " + timeOfDay)
	}

}
