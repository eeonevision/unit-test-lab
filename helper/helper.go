package helper

import "time"

type DayType string

const (
	Night     DayType = "Night"
	Morning   DayType = "Morning"
	Afternoon DayType = "Afternoon"
	Evening   DayType = "Evening"
)

func GetTimeOfDay(time time.Time) DayType {
	if time.Hour() >= 0 && time.Hour() < 6 {
		return Night
	}
	if time.Hour() >= 6 && time.Hour() < 12 {
		return Morning
	}
	if time.Hour() >= 12 && time.Hour() < 18 {
		return Afternoon
	}
	return Evening
}
